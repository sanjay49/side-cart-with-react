var webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
var config = {
  module: {},
};
module.sorceEnable = false;

returnAll = (watchFile) => {
  var sideCart = Object.assign({}, config, {
    context: __dirname,
    entry: ["@babel/polyfill", "./react/index.js"],
    watch: watchFile,
    output: {
      path: __dirname,
      filename: "theme/assets/react.js",
    },
    devServer: {
      historyApiFallback: true,
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          use: {
            loader: "babel-loader",
            options: {
              presets: ["@babel/preset-env", "@babel/preset-react"],
              plugins: ["@babel/plugin-proposal-class-properties"],
            },
          },
        },
        {
          test: /\.jsx?$/,
          use: {
            loader: "babel-loader",
            options: {
              presets: ["@babel/preset-env", "@babel/preset-react"],
              plugins: ["@babel/plugin-proposal-class-properties"],
            },
          },
        },
        {
          test: /\.(png|j?g|svg|gif)?$/,
          use: "file-loader",
        },
        {
          test: /\.scss$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                // you can specify a publicPath here
                // by default it use publicPath in webpackOptions.output
                publicPath: __dirname,
              },
            },
            "css-loader",
            "sass-loader",
          ],
        },
      ],
    },
  });
  return [sideCart];
};
// Return Array of Configurations
module.exports = (env, argv) => {
  let watchFile = false;
  if (argv.mode === "development") {
    watchFile = true;
  }
  return returnAll(watchFile);
};
