import React, { useState, useEffect } from "react";
import MoneyFormat from "./MoneyFormat.jsx";

function SideCart() {
  const [cartdata, setcartdata] = useState([]);

  useEffect(() => {
    window.getCartData();
  },[]);

  window.getCartData = async () => {
    try {
      const result = await fetch("/cart.json");
      if (result.status === 200) {
        const cart = await result.json();
        setcartdata(cart);
      }
    } catch (error) {
      console.log(error);
    }
  };

  // close side cart
  const closeCart = () => {
    let reactSideCart = document.querySelector(".react-side-cart");
    if (reactSideCart) {
      reactSideCart.classList.add("hide");
    }
  };

  const updateCartItem = async (id, newQty) => {
    let formData = {
      updates: {
        [parseInt(id)]: newQty,
      },
    };

    let info = fetch("/cart/update.js", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    })
      .then((response) => response.json().data)
      .then((data) => {
        getCartData();
        return data;
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  const listItems = cartdata?.items?.map((item) => (
    <div key={item.id} data-itemid={item.id} className="cart-item">
      <div className="image-wrap">
        <img src={item.image} alt="" />
      </div>
      <div className="text-wrap">
        <div className="title-price">
          <div className="title">
            <h3>{item.product_title}</h3>
            {item?.options_with_values?.map((option) => (
              <p key={option.name}>
                <strong>{option.name}:-</strong>
                {option.value}
              </p>
            ))}
          </div>
          <div className="item-price">
            <p>
              <MoneyFormat cents={item.price * item.quantity} />
            </p>
          </div>
        </div>
        <div className="plus-minus-update">
          <div className="plus-minus">
            <div
              onClick={() => updateCartItem(item.id, item.quantity - 1)}
              className="minus operator"
            >
              -
            </div>
            <input
              className="qty-input"
              readOnly
              value={item.quantity}
              type="text"
            />
            <div
              onClick={() => updateCartItem(item.id, item.quantity + 1)}
              className="plus operator"
            >
              +
            </div>
          </div>
        </div>
        <div onClick={() => updateCartItem(item.id, 0)} className="remove-btn">
          remove
        </div>
      </div>
    </div>
  ));

  return (
    <>
      <div className="react-side-cart hide">
        <div className="side-cart-top">
          <div className="close-icon" onClick={closeCart}>
            <img
              src="https://cdn.shopify.com/s/files/1/0114/0621/3220/files/side-cart-close-icon.svg?v=8501096318584868990"
              alt="close-icon"
            />
          </div>
          <div className="your-cart-title">Your cart</div>
          <div className="cart-count">
            {cartdata.item_count} {cartdata.item_count > 1 ? "items" : "item"}
          </div>
        </div>
        {cartdata ? (
          cartdata.item_count == 0 ? (
            <div className="emply-view">
              <p>Your cart is empty..🥲</p>
            </div>
          ) : (
            <div className="cart-items">{listItems}</div>
          )
        ) : (
          <img
            className="item-loading"
            src="https://c.tenor.com/tEBoZu1ISJ8AAAAC/spinning-loading.gif"
            alt="spinning-loading"
          />
        )}
        <div className="cart-and-checkout">
          <a className="cart-btn" href="/cart">
            Go To Cart
          </a>
          <a className="checkout-btn" href="/checkout">
            Checkout
          </a>
        </div>
      </div>
    </>
  );
}

export default SideCart;
